#!/bin/bash
# Helper functions for easy scripting of gitlab api calls
#
# Issues:
# - Since everything is done with env vars, it is possible to build
#   a result that is too big. If that happens, generate smaller requests
#   or pipe the results.

# Expects that GITLAB_TOKEN is set to a valid token
GITLAB_API_BASE=${GITLAB_API_BASE:="https://gitlab.com/api/v4"}
GITLAB_API_TRACE=${GITLAB_API_TRACE:=true}

# GitLab APIs often take uriencoded projects, this helper makes it easier to construct them
function uriencode ()
{ 
  jq -nr --arg v "$1" '$v|@uri'
}

# Take a prefix, VERB and API call
# The prefix defines an environment variable namespace into which results are placed.
# Return a bunch of useful env vars for the caller's use, including:
# <prefix>_BODY with the body of the response, caller can echo it and jq process it
# <prefix>_HEADERS with all the headers
# <prefix>_HDR_<foo> For every x-* and ratelimit-* header, put them their own env var
# <prefix>_HDR_x_next_page='2'
# <prefix>_HDR_x_page='1'
# <prefix>_HDR_x_prev_page=''
# <prefix>_HDR_x_total='643'
# <prefix>_HDR_x_total_pages='33'
# <prefix>_HDR_ratelimit_limit='600'
# <prefix>_HDR_ratelimit_observed='2'
# <prefix>_HDR_ratelimit_remaining='598'
# <prefix>_HDR_ratelimit_reset='1602797942'
# <prefix>_HDR_ratelimit_resettime='Thu, 15 Oct 2020 21:39:02 GMT'
# <prefix>_STATUS_CODE containing the http status code of the response
# Note that <prefix> is both the supplied $1 and GITLABAPI_LAST, giving you the ability
# to reference either a named result or the last value returned from the api.
#
# This form of the command is the most useful for scripts since it allows easy access to
# all the results.
function gitlab_api_env () {
  local PREFIX="$1"
  local VERB="$2"
  local API_ENDPOINT="$3"

  # Clear out the env var namespace so they can't be confused with results
  eval $(env | grep -e "^${PREFIX}_" -e "^GITLABAPI_LAST_" | cut -d= -f1 | xargs echo unset)

  # Get the results
  local RESP=$(curl -sSi --retry 2 --request $VERB --header "PRIVATE-TOKEN: $GITLAB_TOKEN" "${GITLAB_API_BASE}${API_ENDPOINT}")
  export GITLABAPI_LAST_HEADERS=$(echo "$RESP" | tr -d '\r' | sed '/^[[:space:]]*$/,$d' )
  export GITLABAPI_LAST_BODY=$(echo "$RESP" | sed '1,/^[[:space:]]*$/d' )
  export GITLABAPI_LAST_STATUS_CODE=$(echo "$RESP" | head -1 | cut -d" " -f2)

  eval $(echo "$GITLABAPI_LAST_HEADERS" | tr -d '\r' | grep : | grep -e '^x-' -e '^ratelimit-'| sed -e "s/: */='/1" -e "s/$/'/g" -e 's/^/export GITLABAPI_LAST_HDR_/g' | tr '-' '_')
  # Construct the env vars to return the results
  eval $(echo "$GITLABAPI_LAST_HEADERS" | tr -d '\r' | grep : | grep -e '^x-' -e '^ratelimit-'| sed -e "s/: */='/1" -e "s/$/'/g" -e 's/^/export ${PREFIX}_HDR_/g' | tr '-' '_')
  eval "export ${PREFIX}_HEADERS=\"\$GITLABAPI_LAST_HEADERS\""
  eval "export ${PREFIX}_BODY=\"\$GITLABAPI_LAST_BODY\""
  eval "export ${PREFIX}_STATUS_CODE=\"\$GITLABAPI_LAST_STATUS_CODE\""
  #env | grep "^${PREFIX}_" | sort >&2
  $GITLAB_API_TRACE && echo ">>> $VERB $API_ENDPOINT $GITLABAPI_LAST_STATUS_CODE -> $PREFIX (t=${GITLABAPI_LAST_HDR_x_total:-} p${GITLABAPI_LAST_HDR_x_page:-}/${GITLABAPI_LAST_HDR_x_total_pages:-} q=${GITLABAPI_LAST_HDR_ratelimit_remaining:-} ${GITLABAPI_LAST_HDR_x_runtime:-}s)" >&2 || true
}

# This form is the mose useful for more one-shot and ad-hoc uses.
function gitlab_api () {
  local VERB="$1"
  local API_ENDPOINT="$2"
  local EXPECTED_STATUS_CODE_REGEX="${3:-2[0-9][0-9]}"
  
  gitlab_api_env "_" "$VERB" "${API_ENDPOINT}"

  if ! echo "$GITLABAPI_LAST_STATUS_CODE" | grep -E "$EXPECTED_STATUS_CODE_REGEX" >/dev/null; then
    echo "ERROR: Expected HTTP $EXPECTED_STATUS_CODE_REGEX but got HTTP $GITLABAPI_LAST_STATUS_CODE. Aborting" >&2
    echo -n "$GITLABAPI_LAST_BODY" >&2
    return 1
  fi
  echo -n "$GITLABAPI_LAST_BODY"
}

# Walk through all pages and put a concatenated body in on stdout
# All the other usual variables are present, but only for the last api call
# Takes the same args as gitlab_api_env plus status-code-regex, which defaults
# to any 2xx http status code being considered a success, otherwise an error
# is returned.
function gitlab_api_all_pages () {
  local PREFIX="$1"
  local VERB="$2"
  local API_ENDPOINT="$3"
  local EXPECTED_STATUS_CODE_REGEX="${4:-2[0-9][0-9]}"
  
  local sep="?"
  if echo "$API_ENDPOINT" | grep '?' >/dev/null; then
    sep="&"
  fi

  for (( page=1, GITLABAPI_LAST_HDR_x_total_pages=1 ; page<=$GITLABAPI_LAST_HDR_x_total_pages; page++ )); do
    gitlab_api_env "$PREFIX" "$VERB" "${API_ENDPOINT}${sep}page=${page}"
    if ! echo "$GITLABAPI_LAST_STATUS_CODE" | grep -E "$EXPECTED_STATUS_CODE_REGEX" >/dev/null; then
      echo "ERROR: Expected HTTP $EXPECTED_STATUS_CODE_REGEX but got HTTP $GITLABAPI_LAST_STATUS_CODE. Aborting" >&2
      echo -n "$GITLABAPI_LAST_BODY" >&2
      return 1
    fi
    echo -n "$GITLABAPI_LAST_BODY"
  done
}
